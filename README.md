# Package Laravel Filter Model

## Install
### Step 1: install package
```shell script
composer require quangphuc/pea-filter-model
```

### Step 2: Migrate database
```shell script
php artisan migrate
```
## Usage


### Step 1: install package
Using trait HasFilter in your Eloquent model

Now Your model has a relationship with name `filters`

![Step 1](https://gitlab.com/p-platform/pea-filter-model-laravel/-/raw/master/.readme/step1.png "Logo Title Text 1")
