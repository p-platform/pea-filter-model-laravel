<?php

namespace QuangPhuc\PeaFilterModel\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Filter
 * @package App\Models
 * @property string $name
 * @property integer $category_id
 * @property FilterCategory $category
 * @property FilterValue[] $values
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Filter extends Model
{
    use HasFactory;

    public function category() {
        return $this->belongsTo(FilterCategory::class, 'category_id');
    }

    public function values() {
        return $this->hasMany(FilterValue::class, 'filter_id');
    }
}
