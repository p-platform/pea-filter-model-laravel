<?php

namespace QuangPhuc\PeaFilterModel\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class FilterCategory
 * @package QuangPhuc\PeaFilterModel\Models
 * @property string $name
 * @property Filter[] $filters
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class FilterCategory extends Model
{
    use HasFactory;

    public function filters() {
        return $this->hasMany(Filter::class, 'category_id');
    }
}
