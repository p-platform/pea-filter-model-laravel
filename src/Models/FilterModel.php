<?php

namespace QuangPhuc\PeaFilterModel\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class FilterModel
 * @package QuangPhuc\PeaFilterModel\Models
 * @property integer $filter_id
 * @property string $model_name
 * @property integer $model_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class FilterModel extends Model
{
    use HasFactory;
}
