<?php

namespace QuangPhuc\PeaFilterModel\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class FilterValue
 * @package QuangPhuc\PeaFilterModel\Models
 * @property string $name
 * @property integer $filter_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class FilterValue extends Model
{
    use HasFactory;

    public function filter() {
        return $this->belongsTo(Filter::class, 'filter_id');
    }
}
