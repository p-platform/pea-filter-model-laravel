<?php
/**
 * HasFilter.php
 * ${CARET}
 *
 * [] - [quang_phuc] - [04/10/2020]
 */

namespace QuangPhuc\PeaFilterModel\Support;


use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use QuangPhuc\PeaFilterModel\Models\Filter;
use QuangPhuc\PeaFilterModel\Models\FilterModel;

trait HasFilter {
    /**
     * @param      $related
     * @param null $table
     * @param null $foreignPivotKey
     * @param null $relatedPivotKey
     * @param null $parentKey
     * @param null $relatedKey
     * @param null $relation
     *
     * @return BelongsToMany
     * @task
     * @since 04/10/2020
     * @author quang_phuc
     */
    abstract function belongsToMany($related, $table = null, $foreignPivotKey = null, $relatedPivotKey = null, $parentKey = null, $relatedKey = null, $relation = null);

    public function filters() {
        return $this->belongsToMany(Filter::class, FilterModel::class, 'model_id', 'filter_id')->where('filter_model.model_name', '=', self::class);
    }
}
