<?php
/**
 * HasFilterInterface.php
 * ${CARET}
 *
 * [] - [quang_phuc] - [04/10/2020]
 */

namespace QuangPhuc\PeaFilterModel\Support;

use QuangPhuc\PeaFilterModel\Models\Filter;

/**
 * Interface HasFilterInterface
 * @package QuangPhuc\PeaFilterModel\Support
 * @property Filter[] $filters
 */
interface HasFilterInterface {

}
