<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_value', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('filter_id')->nullable();
            $table->timestamps();

            $table->foreign('filter_id')->references('id')->on('filter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filter_value');
    }
}
