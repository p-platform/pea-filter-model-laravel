<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilterModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_model', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('filter_id')->nullable();
            $table->unsignedInteger('model_id')->nullable();
            $table->string('model_name');
            $table->timestamps();

            $table->index(['model_name']);
            $table->foreign('filter_id')->references('id')->on('filter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filter_model');
    }
}
